$(document).ready(function() {

	


	//Menu
	menu();

	function menu() {
			var iconMenu = $('.menu');
			iconMenu.click(function(e) {
				e.stopPropagation();
				var nav_xs = $('.navigation');
				nav_xs.toggleClass('open');
			});
			$('.nav ul').on("click", function (e) {
			   e.stopPropagation();
			});
			//$(document).click(function() {
			//	$('.navigation').removeClass('open');
			//});
		}

	//Tab
	(function($) {

        'use strict';

        $(document).on('show.bs.tab', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
            var $target = $(e.target);
            var $tabs = $target.closest('.nav-tabs-responsive');
            var $current = $target.closest('li');
            var $parent = $current.closest('li.dropdown');
            $current = $parent.length > 0 ? $parent : $current;
            var $next = $current.next();
            var $prev = $current.prev();
            var updateDropdownMenu = function($el, position) {
                $el
                    .find('.dropdown-menu')
                    .removeClass('pull-xs-left pull-xs-center pull-xs-right')
                    .addClass('pull-xs-' + position);
            };

            $tabs.find('>li').removeClass('next prev');
            $prev.addClass('prev');
            $next.addClass('next');

            updateDropdownMenu($prev, 'left');
            updateDropdownMenu($current, 'center');
            updateDropdownMenu($next, 'right');
        });

    })(jQuery);


    //MAP
    function init_map() {
        var var_location = new google.maps.LatLng(45.430817, 12.331516);

        var var_mapoptions = {
            center: var_location,
            zoom: 14,
            scrollwheel: false
        };

        var var_marker = new google.maps.Marker({
            position: var_location,
            map: var_map,
            title: "Venice"
        });

        var var_map = new google.maps.Map(document.getElementById("map-container"),
            var_mapoptions);

        var_marker.setMap(var_map);

    }

    google.maps.event.addDomListener(window, 'load', init_map);
		
	//Wrapper
	// wrappers();

	// function wrappers() {
	// 	var windheight = $(window).height();
	// 	var headheight = $('.app-header').outerHeight();
	// 	var footheight = $('.app-footer').outerHeight();
	// 	var wrap = $('.wrapper');
	// 	wrap.css({
	// 		'min-height': windheight,
	// 		'padding-bottom': footheight - 2,
	// 		'padding-top': headheight - 2,
	// 		'position': 'relative',
	// 	})
	// }
	// $(window).resize(function() {
	// 	wrappers();
	// });
	
	//Fullpage
	// fullpage();

	// function fullpage() {
	// 	var windheight = $(window).height();
	// 	var headheight = $('.app-header').outerHeight();
	// 	var footheight = $('.app-footer').outerHeight();
	// 	var wrap = $('.fullpage');
	// 	wrap.css({
	// 		'height': windheight,
	// 		'padding-bottom': footheight - 0,
	// 		'padding-top': headheight - 0,
	// 		'position': 'relative',
	// 	})
	// }
	// $(window).resize(function() {
	// 	fullpage();
	// });
	
	
    


    

	
	
	
});